class Person {
  final String name;
  final String imageUrl;
  final String birthYear, deathYear;
  String biography;

  Person(this.name, this.imageUrl, this.birthYear, this.deathYear, this.biography);

  void updateBiography(String newBiography) => biography = newBiography;
}