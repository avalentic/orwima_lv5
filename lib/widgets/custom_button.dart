import 'package:flutter/material.dart';

Widget customButton(String text, Function onPressed) => FlatButton(
              onPressed: onPressed,
              color: Color(0xffd7d7d7),
              child: Text(text),
            );