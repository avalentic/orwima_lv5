import 'package:flutter/material.dart';
import 'package:lv1/models/person.dart';

class PersonRow extends StatefulWidget {
  final Person person;

  PersonRow(this.person);

  @override
  _PersonRowState createState() => _PersonRowState();
}

class _PersonRowState extends State<PersonRow> {
  bool imageVisible = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.all(4),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          InkWell(
            onTap: () => _hideImage(),
            child: Container(
              height: 70,
              width: 70,
              child: AnimatedOpacity(
                opacity: imageVisible ? 1.0 : 0.0,
                duration: Duration(milliseconds: 300),
                child: Image.asset(
                  widget.person.imageUrl,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 8, right: 12),
              height: 70,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.person.name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    "${widget.person.birthYear} - ${widget.person.deathYear}",
                    style: TextStyle(
                      color: Color(0xffd7d7d7),
                      fontSize: 12,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 20,
                      child: Text(
                        widget.person.biography,
                        style: TextStyle(
                          fontSize: 14,
                        ),
                        overflow: TextOverflow.clip,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _hideImage() {
    setState(() {
      imageVisible = false;
    });
  }
}
