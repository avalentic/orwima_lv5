import 'package:flutter/material.dart';
import 'package:lv1/models/person.dart';
import 'package:lv1/widgets/custom_button.dart';
import 'package:lv1/widgets/person_row.dart';
import 'package:lv1/constants/quotes.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Person> _persons;
  int _radioValue = -1;
  TextEditingController _descriptionController;

  @override
  void initState() {
    super.initState();
    _persons = [
      Person(
          "Jules Bianchi",
          "assets/images/jules_bianchi.jpeg",
          "1989",
          "2015",
          "Jules Bianchi bio je francuski vozač. Nakon nesreće na stazi Suzuka u Japanu u kojoj je udario u bager svojim bolidom, bio je 9 mjeseci u komi. Njegov otac Philippe Bianchi je zadnjih dana gubio nadu za njegov oporavak. Dana 17. jula 2015. je preminuo."),
      Person(
          "Charles Leclerc",
          "assets/images/charles_leclerc.jpg",
          "1997",
          "//",
          "Charles Leclerc (Monte Carlo, Monako, 16. listopada, 1997.) je monegaški vozač automobilskih utrka."),
      Person("Lando Norris", "assets/images/lando_norris.jpg", "1999", "//",
          "Lando Norris (Bristol, 13. novembar 1999.) je britanski vozač."),
    ];
    _descriptionController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff3f3f3),
      appBar: AppBar(
        backgroundColor: Color(0xff1bbc9b),
        title: Text("Famous F1 drivers"),
        centerTitle: false,
      ),
      body: Column(
        children: <Widget>[
          _buildTopRow(),
          Container(
            height: 400,
            child: ListView.builder(
              itemCount: _persons.length,
              itemBuilder: (BuildContext context, int index) {
                return PersonRow(_persons[index]);
              },
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: _buildBottomContainer(),
            ),
          ),
        ],
      ),
    );
  }

  void _radioChange(int newValue) {
    setState(() {
      _radioValue = newValue;
    });
  }

  void _editDescription(String newDescription) {
    setState(() {
      _persons[_radioValue].updateBiography(newDescription);
      _radioValue = -1;
      _descriptionController.clear();
    });
  }

  void _showRandomQuote() {
    String randomQuote = Quotes().getRandomQuote;
    debugPrint(randomQuote);
    Fluttertoast.showToast(
        msg: randomQuote,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
    );
  }

  _buildTopRow() => Container(
        margin: const EdgeInsets.only(left: 36),
        child: Row(
          children: <Widget>[
            customButton("INSPIRATION", () => _showRandomQuote()),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 12),
                height: 35,
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Color(0xff1bbc9b), width: 2)),
                ),
                child: Center(
                  child: Text(
                    "TOP 3",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  _buildBottomContainer() => Container(
        height: 150,
        margin: const EdgeInsets.only(right: 4),
        padding: const EdgeInsets.only(left: 8),
        decoration: BoxDecoration(
          color: Color(0xfffafafa),
          border: Border(right: BorderSide(color: Color(0xff5350b9), width: 3)),
        ),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _descriptionController,
              decoration: InputDecoration(
                hintText: "Enter new description",
              ),
            ),
            customButton("EDIT DESCRIPTION", () => _editDescription(_descriptionController.text)),
            Row(
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: _radioValue,
                  onChanged: (newValue) => _radioChange(newValue),
                ),
                Text("First person"),
                Radio(
                  value: 1,
                  groupValue: _radioValue,
                  onChanged: (newValue) => _radioChange(newValue),
                ),
                Text("Second person"),
                Radio(
                  value: 2,
                  groupValue: _radioValue,
                  onChanged: (newValue) => _radioChange(newValue),
                ),
                Text("Third person"),
              ],
            ),
          ],
        ),
      );
}
