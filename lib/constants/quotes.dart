import 'dart:math';

class Quotes {
  final List<String> _quotes = [
    '"I honesty thought it was over." - Jules Bianchi',
    '"It has been easy to settle in." - Jules Bianchi',
    '"It is I, Leclerc." - Charles Leclerc',
    '"I had a much better view with the halo than I expected. - Lando Norris"',
  ];

  String get getRandomQuote {
    Random random = new Random();
    return _quotes[random.nextInt(_quotes.length)];
  }
}